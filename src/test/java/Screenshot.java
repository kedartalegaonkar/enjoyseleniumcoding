import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Screenshot {

	public static void main(String[] args) throws IOException, Exception {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

		WebDriver driver = new ChromeDriver();		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://demo.guru99.com/test/newtours/");
		driver.manage().window().maximize();

		driver.findElement(By.name("userName")).sendKeys("kedar");
		driver.findElement(By.name("password")).sendKeys("kedar");
				
		TakesScreenshot scrShot  = ((TakesScreenshot) driver);
		File screenshot =scrShot.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File("screenshots/homePageScreenshot1.png"));
		Thread.sleep(3000);
		driver.findElement(By.name("submit")).click();
		driver.close();

	}

}
