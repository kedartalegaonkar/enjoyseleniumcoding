import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class HTMLUnitDriver {

	public static void main(String[] args) {
		
		WebDriver driver = new HtmlUnitDriver();
		  driver.get("https://www.google.com/");
		  driver.findElement(By.name("q")).sendKeys("Test");
		  String titleString=driver.getTitle();
		  System.out.println("Page title is: "+titleString);
		  String urlString=driver.getCurrentUrl();
		  System.out.println("Page URL is: "+urlString);
		  driver.quit();
	}
	
	static WebDriver driver;
	public static boolean waitForPageLoad(WebDriver driver, int timeout) {
		
	ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) 
			{
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
	WebDriverWait wait = new WebDriverWait(driver, timeout);
	boolean flag=wait.until(pageLoadCondition);
	return flag;

	}

}
