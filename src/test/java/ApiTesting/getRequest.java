package ApiTesting;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class getRequest {
	
	@Test
	public void getResponce() {
		Response resp=RestAssured.get("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		System.out.println("code: "+resp.getStatusCode());
		System.out.println("Respoce as String: "+resp.asString());
		System.out.println("Time in milli seconds: "+resp.getTime());
		System.out.println("Header Deatils: "+resp.getHeaders().toString());
	}

}
