package ApiTesting;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;

public class getRequest2 {
	
	@Test
	public void getResponce() {
		//Response resp=get("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		String url="https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
		int code=get(url).getStatusCode();
		System.out.println("code: "+code);
		System.out.println("Respoce as String: "+get(url).asString());
		System.out.println("Time in milli seconds: "+get(url).getTime());
		System.out.println("Header Deatils: "+get(url).getHeaders().toString());
	}
}
