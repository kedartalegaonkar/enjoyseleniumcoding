package ApiTesting;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNG {
	
	@Test
	public void testCase1() {
		System.out.println("Under test case 1");
	}	
	
	@Test
	public void testCase2() {
		System.out.println("Under test case 2");
		Assert.assertTrue(true);
		
	}	
	
	@Test
	public void testCase3() {
		System.out.println("Under test case 3");
	}	

}
