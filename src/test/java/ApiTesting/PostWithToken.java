package ApiTesting;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PostWithToken {
	
	@Test
	public void accessApi() 
	{
		
		Response resp=RestAssured.given().
		formParam("client_id", "KedarApp").
		formParam("client_secret", "7d6b83029ee6055e6f31a4711dd841ac").
		formParam("grant_type", "client_credentials").
		post("http://coop.apps.symfonycasts.com/token");
		
		System.out.println(resp.jsonPath().prettify());
		System.out.println(resp.jsonPath().get("access_token"));
		String token=resp.jsonPath().get("access_token");		
		
		  RestAssured.given().auth().
		  oauth2(token).
		  post("http://coop.apps.symfonycasts.com/api/1605/chickens-feed");
		  System.out.println(resp.getStatusCode());		
	}
}
