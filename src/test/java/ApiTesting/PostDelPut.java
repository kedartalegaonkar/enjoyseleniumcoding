package ApiTesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostDelPut {
	
	@Test(enabled = false)
	public void postRequest() 
	{
		RestAssured.baseURI ="https://reqres.in/api/users";
		RequestSpecification request=RestAssured.given();
		request.header("Content-Type", "application/json");  // Header information
		JSONObject json=new JSONObject();
		json.put("name", "Naredra"); // Body
		json.put("job", "leader");  // Body
		request.body(json.toJSONString());
		Response resp=request.post(RestAssured.baseURI);
		System.out.println("code: "+resp.getStatusCode());
	}
	
	@Test(enabled = false)
	public void putRequest() {
		RestAssured.baseURI ="https://reqres.in/api/users/2";
		RequestSpecification request=RestAssured.given();
		request.header("Content-Type", "application/json");
		JSONObject json=new JSONObject();
		json.put("name", "morpheus");
		json.put("job", "zion resident");
		request.body(json.toJSONString());
		Response resp=request.put(RestAssured.baseURI);
		System.out.println("code: "+resp.getStatusCode());
	}
	
	@Test
	public void deleteRequest() {
		Response resp1=RestAssured.delete("https://reqres.in/api/users/2");
		System.out.println("code1: "+resp1.getStatusCode());
	}

}
